﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace TestApp
{
    class FileHandler
    {
        public string Dir { get; set; }
        public string FileName { get; set; }
        public string FileText { get; set; }

        private int countOfScannedFiles = 0;
        
        private void Find(string pathDir, TextBox textBox, TreeView treeView, ManualResetEvent manualEvent)
        {
            DirectoryInfo dirInf;
            try
            {
                manualEvent.WaitOne(); 

                dirInf = new DirectoryInfo(pathDir);
                DirectoryInfo[] w = dirInf.GetDirectories();

                foreach (var item in w)
                {
                    if (item.Attributes.Equals(FileAttributes.System | FileAttributes.Hidden | FileAttributes.Directory))
                    {
                        continue;
                    }
                    Find(item.FullName, textBox, treeView, manualEvent);
                }

                FindFile(dirInf, textBox, treeView, manualEvent);
            }
            catch (Exception)
            {
                return;
            }
        }

        private void FindFile(DirectoryInfo dir, TextBox textBox, TreeView treeView, ManualResetEvent manualEvent)
        {
            string[] arrFile = Directory.EnumerateFiles(dir.FullName).ToArray();

            for (int n = 0; n < arrFile.Length; n++)
            {
                manualEvent.WaitOne();

                string fName = Path.GetFileName(arrFile[n]);

                Form1.SetText(textBox, String.Format("Проверяется файл: {0}\r\nВсего проверенно файлов: {1}\r\n", fName, countOfScannedFiles));

                if (fName.Contains(FileName))
                {
                    if (FindFileText(arrFile[n], manualEvent))
                    {
                        Form1.BuildTree(treeView, arrFile[n]);
                    }
                }

                countOfScannedFiles++;
            }
        }

        public bool FindFileText(string file, ManualResetEvent manualEvent)
        {
            try
            {
                using (StreamReader reader = new StreamReader(file, Encoding.Default))
                {
                    while (!reader.EndOfStream)
                    {
                        manualEvent.WaitOne();
                        string str = reader.ReadLine();

                        if (str.ToLower().Contains(FileText.ToLower()))
                        {
                            return true;
                        }
                    }
                }
            }
            catch (Exception)
            {
                return false;
            }

            return false;
        }

        public void StartFind(CancellationToken token, TextBox textBox, TreeView treeView, ManualResetEvent manualEvent)
        {
            if (!token.IsCancellationRequested)
            {
                countOfScannedFiles = 0;
                Find(Dir, textBox, treeView, manualEvent);
            }

            Form1.SetText(textBox, new StringBuilder(textBox.Text).AppendLine("Поиск завершен").ToString());
        }
    }
}
