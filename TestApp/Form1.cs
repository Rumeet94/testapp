﻿using System;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TestApp
{
    public partial class Form1 : Form
    {
        private readonly FileHandler data = new FileHandler();
        private CancellationTokenSource cancellationTokenSource;
        private ManualResetEvent manualEvent;

        private bool pause = false;

        public Form1()
        {
            InitializeComponent();
            GetOldParams();
        }

        private void ButtonStart_Click(object sender, EventArgs e)
        {
            if (CheckParams())
            {
                InitializeParams();
                treeViewResult.Nodes.Clear();

                buttonPause.Enabled = true;
                buttonStop.Enabled = true;

                cancellationTokenSource = new CancellationTokenSource();
                manualEvent = new ManualResetEvent(true);

                Task.Factory.StartNew(() => data.StartFind(cancellationTokenSource.Token, textBoxStatus, treeViewResult, manualEvent)
                                          , TaskCreationOptions.LongRunning);
            }
            else
            {
                return;
            }
        }

        private void ButtonStop_Click(object sender, EventArgs e)
        {
            if (manualEvent != null)
            {
                manualEvent.Reset();

                buttonPause.Enabled = false;
                buttonPause.Text = "Pause";
                buttonStop.Enabled = false;

                pause = false;

                cancellationTokenSource?.Cancel();
            }
        }

        private void ButtonPause_Click(object sender, EventArgs e)
        {
            if (!pause)
            {
                manualEvent.Reset();
                pause = true;
                buttonPause.Text = "Resume";
            }
            else
            {
                manualEvent.Set();
                pause = false;
                buttonPause.Text = "Pause";
            }
        }

        private void InitializeParams()
        {
            data.Dir = textBoxDir.Text;
            data.FileName = textBoxFileName.Text;
            data.FileText = textBoxFileText.Text;

            Properties.Settings.Default.dirOld = textBoxDir.Text;
            Properties.Settings.Default.fileNameOld = textBoxFileName.Text;
            Properties.Settings.Default.fileTextOld = textBoxFileText.Text;

            Properties.Settings.Default.Save();

            pause = false;
        }

        private void GetOldParams()
        {
            textBoxDir.Text = Properties.Settings.Default.dirOld;
            textBoxFileName.Text = Properties.Settings.Default.fileNameOld;
            textBoxFileText.Text = Properties.Settings.Default.fileTextOld;
        }

        private bool CheckParams()
        {
            StringBuilder builder = new StringBuilder();
            bool check = true;

            if (!Regex.IsMatch(textBoxDir.Text, @"^.:\w*", RegexOptions.IgnoreCase))
            {
                builder.AppendLine("Дирректория указана некоректно.");
                check = false;
            }

            if (textBoxFileName.Text.Equals(""))
            {
                builder.AppendLine("Имя файла указано не корректно.");
                check = false;
            }

            if (textBoxFileText.Text.Equals(""))
            {
                builder.AppendLine("Текст, содержащийся в файле, не указан.");
                check = false;
            }

            textBoxStatus.Text = builder.ToString();
            return check;
        }

        private delegate void TextBoxDelegate(TextBox Info, string Text);
        public static void SetText(TextBox textBox, string text)
        {
            if (textBox.InvokeRequired)
            {
                TextBoxDelegate tbDelegate = new TextBoxDelegate(SetText);
                textBox.Invoke(tbDelegate, new object[] { textBox, text });
            }
            else
            {
                textBox.Text = text;
            }
        }

        private delegate void TreeViewDelegate(TreeView Info, string Text);
        public static void BuildTree(TreeView treeView, string path)
        {
            if (treeView.InvokeRequired)
            {
                TreeViewDelegate tvDelegate = new TreeViewDelegate(BuildTree);
                treeView.Invoke(tvDelegate, new object[] { treeView, path });
            }
            else
            {
                var childs = treeView.Nodes;
                foreach (var part in path.Split(Path.DirectorySeparatorChar))
                {
                    childs = FindOrCreateNode(childs, part).Nodes;
                }
            }
        }

        private static TreeNode FindOrCreateNode(TreeNodeCollection coll, string name)
        {
            var found = coll.Find(name.ToLower(), false);
            if (found.Length > 0) return found[0];
            return coll.Add(name.ToLower(), name);
        }
    }
}
